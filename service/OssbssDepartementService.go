package service

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"projek/models"
	"projek/properties"
	"strconv"
	"sync"
	"time"

	"projek/gitlab.com/notula/go-tools/dbms"
	go_tools "projek/gitlab.com/notula/go-tools2"

	"github.com/nats-io/nats.go"
	"go.uber.org/zap"
)

type OssbssDepartementService struct {
	nats       *nats.Conn
	dbms       *dbms.DatabaseRepository
	logger     *go_tools.Logger
	properties *properties.ServiceProperties
}

func (this *OssbssDepartementService) Init(properties *properties.ServiceProperties) {
	this.properties = properties
	var err error
	this.logger, err = go_tools.LoggerRotateGenerator(this.properties.Logging.OutputDir, this.properties.Logging.ErrorOutputDir, this.properties.Logging.Name, this.properties.Logging.Debug)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	this.nats, err = nats.Connect(this.properties.Nats.Address)
	if err != nil {
		this.logger.Error(err.Error())
		os.Exit(1)
	}

	// dbm := dbms.New(this.properties.Database, dbms.POSTGRESQL)
	// e := dbm.Connect()
	// if e != nil {
	// 	this.logger.Info("dbms_connect", zap.String("error", e.Error()))
	// }
	// dbm.Database.LogMode(true)

	subscription, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("departement_list"), this.properties.Topic.GetQueueName("department_list"), this.departementList)
	this.logger.Info("subscribtion", zap.String("subject", subscription.Subject), zap.Bool("status", subscription.IsValid()))

	subscription2, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("department_add"), this.properties.Topic.GetQueueName("department_add"), this.departmentAdd)
	this.logger.Info("subscribtion", zap.String("subject", subscription2.Subject), zap.Bool("status", subscription2.IsValid()))

	subscription3, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("department_edit"), this.properties.Topic.GetQueueName("departement_edit"), this.departmentEdit)
	this.logger.Info("subscribtion", zap.String("subject", subscription3.Subject), zap.Bool("status", subscription3.IsValid()))

	subscription4, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("department_delete"), this.properties.Topic.GetQueueName("department_detele"), this.departmentDelete)
	this.logger.Info("subscribtion", zap.String("subject", subscription4.Subject), zap.Bool("status", subscription3.IsValid()))
}

func (this *OssbssDepartmentService) departementList(msg *nats.Msg) {
	var response = models.ResponseDepartementList{Response: models.Response{Status: "failed"}}
	request := &models.RequestDepartementList{}
	this.logger.Info("department_list", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var packages = models.DepartementList{IdDept: 1, Name: "Produk", Offset: " ", Limit: " ", Total: " "}
		var arr_packages []models.DepartementList
		arr_packages = append(arr_packages, packages)
		if err != nil {
			response.Error = err.Error()
		} else {
			if len(arr_packages) > 0 {
				response.Status = "success"
				response.Packages = arr_packages
			} else {
				response.Error = "service not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("department_list", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}

func (this *OssbssDepartementService) departmentAdd(msg *nats.Msg) {
	var response = models.ResponseDepartmentAdd{Response: models.Response{Status: "failed"}}
	request := &models.RequestDepartmentAdd{}
	this.logger.Info("department_add", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		min := 1
		max := 5
		token := fmt.Sprintf("%x", sha256.Sum256([]byte(request.name+fmt.Sprintf("%x", time.Now()))))
		stringAvatar := strconv.Itoa((rand.Intn(max-min) + min)) + ".png"
		isValid := true
		fmt.Println(stringAvatar)
		if isValid {
			response.Status = "success"
			response.IdToken = token
			go func() {
				this.nats.Subscribe("1")
				// this.registerProcessVeriv
				var wg = sync.WaitGroup{}
				wg.Add(1)
				wg.Wait()
			}()
		} else {
			response.Error = "name department has been used"
			this.nats.Publish("1", []byte(`{"status": "failed", "error":"name department has been used"}`))
			this.nats.FlushTimeout(10 * time.Second)
		}
	} else {
		response.Error = "unrecognize request"
	}
	this.logger.Info("departmnet_add", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, err := json.Marshal(response)
	if err != nil {
		this.logger.Error(err.Error())
	}
	this.nats.Publish(msg.Reply, bytes)
}

func (this *OssbssDepartmentService) departmentEdit(msg *nats.Msg) {
	var response = models.ResponseDepartmentEdit{Response: models.Response{Status: "failed"}}
	request := &models.RequestDepartmentEdit{}
	this.logger.Info("departement_edit", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		response.IdToken = fmt.Sprintf("%x", sha256.Sum256([]byte(request.name+fmt.Sprintf("%x", time.Now()))))
		var isValid bool
		isValid = true
		if isValid {
			response.Status = "success"
		}
	} else {
		response.Error = "data not edit"
	}

	this.logger.Info("department_edit", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}

func (this *OssbssDepartmentService) departmentDelete(msg *nats.Msg) {
	var response = models.ResponseDepartmentDelete{Response: models.Response{Status: "failed"}}
	request := &models.RequestDepartmentdelte{}
	this.logger.Info("departement_delete", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		response.IdToken = fmt.Sprintf("%x", sha256.Sum256([]byte(request.name+fmt.Sprintf("%x", time.Now()))))
		var isValid bool
		isValid = true
		if isValid {
			response.Status = "success"
		}
	} else {
		response.Error = "Error delete data"
	}

	this.logger.Info("department_delete", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}
