package service

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"projek/models"
	"projek/properties"
	"strconv"
	"sync"
	"time"

	"projek/gitlab.com/notula/go-tools/dbms"
	go_tools "projek/gitlab.com/notula/go-tools2"

	"github.com/nats-io/nats.go"
	"go.uber.org/zap"
)

type OssbssUserManagementService struct {
	nats       *nats.Conn
	dbms       *dbms.DatabaseRepository
	logger     *go_tools.Logger
	properties *properties.ServiceProperties
}

func (this *OssbssUserManagementService) Init(properties *properties.ServiceProperties) {
	this.properties = properties
	var err error
	this.logger, err = go_tools.LoggerRotateGenerator(this.properties.Logging.OutputDir, this.properties.Logging.ErrorOutputDir, this.properties.Logging.Name, this.properties.Logging.Debug)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	this.nats, err = nats.Connect(this.properties.Nats.Address)
	if err != nil {
		this.logger.Error(err.Error())
		os.Exit(1)
	}

	// dbm := dbms.New(this.properties.Database, dbms.POSTGRESQL)
	// e := dbm.Connect()
	// if e != nil {
	// 	this.logger.Info("dbms_connect", zap.String("error", e.Error()))
	// }
	// dbm.Database.LogMode(true)

	subscription, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("login"), this.properties.Topic.GetQueueName("login"), this.loginProcess)
	this.logger.Info("subscribtion", zap.String("subject", subscription.Subject), zap.Bool("status", subscription.IsValid()))

	subscription2, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("register"), this.properties.Topic.GetQueueName("register"), this.registerProcess)
	this.logger.Info("subscribtion", zap.String("subject", subscription2.Subject), zap.Bool("status", subscription2.IsValid()))

	subscription3, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("change_pass"), this.properties.Topic.GetQueueName("change_pass"), this.changePassProcess)
	this.logger.Info("subscribtion", zap.String("subject", subscription3.Subject), zap.Bool("status", subscription3.IsValid()))

	subscription4, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("reset_pass"), this.properties.Topic.GetQueueName("reset_pass"), this.resetPasswordProcess)
	this.logger.Info("subscribtion", zap.String("subject", subscription4.Subject), zap.Bool("status", subscription4.IsValid()))
}

func (this *OssbssUserManagementService) loginProcess(msg *nats.Msg) {
	var response = models.ResponseLogin{Response: models.Response{Status: "failed"}}
	request := &models.RequestLogin{}
	this.logger.Info("login_process", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		response.IdToken = fmt.Sprintf("%x", sha256.Sum256([]byte(request.Username+fmt.Sprintf("%x", time.Now()))))
		var isValid bool
		if err != nil {
			response.Error = err.Error()
		} else {
			isValid = true
			if isValid {
				response.Status = "success"
			} else {
				response.Error = "user not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("login_process", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}
func (this *OssbssUserManagementService) registerProcess(msg *nats.Msg) {
	var response = models.ResponseLogin{Response: models.Response{Status: "failed"}}
	request := &models.RequestRegister{}
	this.logger.Info("register_process", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		min := 1
		max := 5
		token := fmt.Sprintf("%x", sha256.Sum256([]byte(request.Username+fmt.Sprintf("%x", time.Now()))))
		stringAvatar := strconv.Itoa((rand.Intn(max-min) + min)) + ".png"
		isValid := true
		fmt.Println(stringAvatar)
		if isValid {
			response.Status = "success"
			response.IdToken = token
			go func() {
				this.nats.Subscribe("1", this.registerProcessVeriv)
				var wg = sync.WaitGroup{}
				wg.Add(1)
				wg.Wait()
			}()
		} else {
			response.Error = "username / email has been used"
			this.nats.Publish("1", []byte(`{"status": "failed", "error":"username / email has been used"}`))
			this.nats.FlushTimeout(10 * time.Second)
		}
	} else {
		response.Error = "unrecognize request"
	}
	this.logger.Info("register_process", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, err := json.Marshal(response)
	if err != nil {
		this.logger.Error(err.Error())
	}
	this.nats.Publish(msg.Reply, bytes)
}

func (this *OssbssUserManagementService) registerProcessVeriv(msg *nats.Msg) {
	var response = models.ResponseLogin{Response: models.Response{Status: "failed"}}
	this.logger.Info("reset_password", zap.String("subject", msg.Subject))
	response.Status = "success"
	this.logger.Info("reset_password", zap.String("subject", msg.Subject))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
	msg.Sub.Unsubscribe()
}

func (this *OssbssUserManagementService) resetPasswordProcess(msg *nats.Msg) {
	var response = models.ResponseLogin{Response: models.Response{Status: "failed"}}
	request := &models.RequestLogin{}
	this.logger.Info("reset_password", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		response.IdToken = fmt.Sprintf("%x", sha256.Sum256([]byte(request.Username+fmt.Sprintf("%x", time.Now()))))
		var isValid bool
		isValid = true
		if isValid {
			response.Status = "success"
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("reset_password", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}
func (this *OssbssUserManagementService) changePassProcess(msg *nats.Msg) {

	var response = models.ResponseLogin{Response: models.Response{Status: "failed"}}
	request := &models.RequestChangePassword{}
	this.logger.Info("change_pass_process", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var isValid bool
		isValid = true
		if isValid {
			response.Status = "success"
		} else {
			response.Error = "password is not changed"
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("change_pass_process", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}
