package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"projek/info"
	"projek/properties"
	"strings"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/nats-io/nats.go"

	go_tools "projek/gitlab.com/notula/go-tools"

	"go.uber.org/zap"
)

var logger *zap.Logger
var conn *nats.Conn

func dirExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func logger_endpoint_ncloud(prop properties.EndPointProperties) *zap.Logger {
	var logoutput []string
	var logerror []string
	t := time.Now()
	s := t.Format("2020-16-09")
	fmt.Println(prop.Logging.OutputPaths)
	logoutput = append(logoutput, prop.Logging.OutputPaths[0]+"endpoint-"+s+".log", prop.Logging.OutputPaths[1])
	logerror = append(logerror, prop.Logging.ErrorOutputPaths[0]+"endpoint-"+s+".log", prop.Logging.ErrorOutputPaths[1])
	// fmt.Println(this.properties.Logging.OutputPaths[0] + "log.log")
	logger, _ := go_tools.LoggerGenerator(logoutput, logerror)
	return logger
}

func RequestHadler(val string, auth bool, timeout time.Duration) func(c echo.Context) error {
	return func(context echo.Context) (err error) {
		response := make(map[string]string)
		req := make(map[string]interface{})
		if err := context.Bind(&req); err != nil {
			logger.Error(err.Error())
			response["status"] = "failed"
			response["error"] = err.Error()
			logger.Error(err.Error())
			return context.JSON(http.StatusInternalServerError, response)
		} else {
			logger.Info("request", zap.String("subject", val), zap.Any("data", req))
			bytes, err := json.Marshal(req)
			msg, err := conn.Request(val, bytes, timeout*time.Millisecond)
			conn.FlushTimeout(time.Second)
			if err != nil {
				logger.Error(err.Error())
				response["status"] = "failed"
				response["error"] = err.Error()
				err = context.JSON(http.StatusInternalServerError, response)
			} else {
				response2 := make(map[string]interface{})
				fmt.Println(string(msg.Data))
				if err := json.Unmarshal(msg.Data, &response2); err == nil {
					logger.Info("result", zap.Any("data", response2))
					err = context.JSON(http.StatusAccepted, response2)
				} else {
					logger.Error(err.Error())
					response["status"] = "failed"
					response["error"] = err.Error()
					err = context.JSON(http.StatusInternalServerError, response)
				}
			}
		}
		return
	}
}

func main() {
	info.PrintHeader()
	prop := properties.EndPointProperties{}
	err := go_tools.Ekstration("configPath", "configName", &prop)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		exists, _ := dirExists(prop.Logging.OutputPaths[0])
		if exists == false {
			merr := os.MkdirAll(prop.Logging.OutputPaths[0], os.ModePerm)
			if merr != nil {
				panic(merr)
			}
		}
		exists, _ = dirExists(prop.Logging.ErrorOutputPaths[0])
		if exists == false {
			merr := os.MkdirAll(prop.Logging.ErrorOutputPaths[0], os.ModePerm)
			if merr != nil {
				panic(merr)
			}
		}
		logger = logger_endpoint_ncloud(prop)
		// logger, _ = go_tools.LoggerGenerator(prop.Logging.OutputPaths, prop.Logging.ErrorOutputPaths)
		conn, _ = nats.Connect(prop.Nats.Address)
		e := echo.New()
		//e.AutoTLSManager.HostPolicy = autocert.HostWhitelist("localhost")
		//e.AutoTLSManager.Cache = autocert.DirCache(".cache")
		e.Use(middleware.Logger())
		e.Use(middleware.Recover())

		//CORS
		e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
			AllowOrigins: []string{"*"},
			AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE}}))
		for request_type, urls := range prop.Topic.Channel {
			for key, _ := range urls {
				if strings.Compare("post", request_type) == 0 {
					logger.Info("bind_endpoint", zap.String("endpoint", prop.Topic.GetChannelRest(key, "post")), zap.String("subject", prop.Topic.GetChannelNats(key, "post")))
					e.POST(prop.Topic.GetChannelRest(key, "post"), RequestHadler(prop.Topic.GetChannelNats(key, "post"), true, time.Duration(prop.Timeout)))
				} else if strings.Compare("get", request_type) == 0 {
					logger.Info("bind_endpoint", zap.String("endpoint", prop.Topic.GetChannelRest(key, "get")), zap.String("subject", prop.Topic.GetChannelNats(key, "get")))
					e.GET(prop.Topic.GetChannelRest(key, "get"), RequestHadler(prop.Topic.GetChannelNats(key, "get"), true, time.Duration(prop.Timeout)))
				}
			}
		}

		e.POST("slack", func(c echo.Context) error {
			var sample map[string]interface{}
			err := c.Bind(&sample)
			if err != nil {
				logger.Error(err.Error())
			} else {
				bytes, _ := json.Marshal(sample)
				logger.Info("slack", zap.String("message", string(bytes)))
			}
			return c.JSON(http.StatusAccepted, "success")
		})
		// logger.Fatal(e.StartAutoTLS(":" + prop.Port).Error())
		//logger.Info("", zap.Any("prop", prop))
		logger.Fatal(e.Start(":" + prop.Port).Error())
		// logger.Fatal(e.StartTLS(":"+prop.Port, prop.CertDir+"cert.pem", prop.CertDir+"key.pem").Error())
	}
}
