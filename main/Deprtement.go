package main

import (
	"encoding/json"
	"fmt"
	go_tools "projek/gitlab.com/notula/go-tools2"
	"projek/info"
	"projek/properties"
	"projek/service"
	"sync"
)

func main() {
	info.PrintHeader()
	properties := properties.ServiceProperties{}
	err := go_tools.Ekstration("configPath", "configName", &properties)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		byt, _ := json.Marshal(properties)

		fmt.Println(string(byt))

		service := service.OssbssDepartementService{}
		service.Init(&properties)
		var wg sync.WaitGroup
		wg.Add(1)
		wg.Wait()
	}
}
