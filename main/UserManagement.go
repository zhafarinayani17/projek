package main

import (
	"encoding/json"
	"fmt"
	"projek/info"
	"projek/properties"
	"projek/service"
	"sync"

	go_tools "projek/gitlab.com/notula/go-tools2"
)

func main() {
	info.PrintHeader()
	properties := properties.ServiceProperties{}
	err := go_tools.Ekstration("configPath", "configName", &properties)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		byt, _ := json.Marshal(properties)

		fmt.Println(string(byt))

		service := service.OssbssUserManagementService{}
		service.Init(&properties)
		var wg sync.WaitGroup
		wg.Add(1)
		wg.Wait()
	}
}
