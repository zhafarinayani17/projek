package models

// Request

type RequestLogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type RequestRegister struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

type RequestReset struct {
	Email string `json:"email"`
}

type RequestChangePassword struct {
	IdToken     string `json:"id_token"`
	CurrentPass string `json:"current_pass"`
	Password    string `json:"password"`
}

type RequestDepartementList struct {
	IdToken string `json:"id_token"`
	Offset  int    `json:"offset,omitempty"`
	Limit   int    `json:"limit,omitempty"`
}

type RequestDepartmentAdd struct {
	IdToken string `json:"id_token"`
	Name    string `json:"name"`
}

type RequestDepartmentEdit struct {
	IdToken string `json:"id_token"`
	Name    string `json:"name"`
	IdDept  int    `json:"id_dept"`
}

type RequestDepartmentDelete struct {
	IdToken string `json:"id_token"`
	IdDept  int    `json:"id_dept"`
}

// Response

type Response struct {
	Error  string `json:"error"`
	Status string `json:"status"`
}
type Depts struct {
	IdDept int    `json:"id_dept"`
	name   string `json:"name"`
}

type ResponseLogin struct {
	Response
	IdToken string `json:"id_token"`
}

type ResponseRegister struct {
	Response
	IdToken string `json:"id_token"`
}
type ResponseChangePassword struct {
	Response
}

type ResponseDepartementList struct {
	Depts
	Offset int `json:"offset,omitempty"`
	Limit  int `json:"limit,omitempty"`
	Total  int `json:"total,omitempty"`
	Response
}

type ResponseDepartmentAdd struct {
	Response
}

type ResponseDepartmentEdit struct {
	Response
}

type ResponseDepartmentDelete struct {
	Response
}

type ResponseBillingPayment struct {
	Response
	IdTransaction int `json:"id_transaction"`
}

type ResponseTutorialList struct {
	Response
	Services []TutorialResult `json:"services"`
}
