package models

type PlanDurationResult struct {
	IdPlan int    `json:"id_plan"`
	Name   string `json:"name"`
	Price  int    `json:"price"`
}

type ServiceListResult struct {
	IdService   int    `json:"id_service"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

type PackageResult struct {
	IdPackage        int                  `json:"id_package"`
	Name             string               `json:"name"`
	TitleDescription string               `json:"title_description"`
	Price            int                  `json:"price"`
	Discount         string               `json:"discount"`
	Type             string               `json:"type"`
	Description      []string             `json:"description"`
	PlanDuration     []PlanDurationResult `json:"plan_duration"`
}

type DepartmentList struct {
	IdDept int    `json:"id_dept"`
	Name   string `json:"name"`
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Total  int    `json:"total"`
}

type DepartmentAdd struct {
	IdToken string `json:"id_token"`
	Name    string `json:"name"`
}

// type PackageListDetail struct {
// 	VolumeBase   PackageBase `json:"volume_base"`
// 	TimeBase     PackageBase `json:"time_base"`
// 	ErrorCount   int         `json:"error_count"`
// 	ApiKey       string      `json:"api_key"`
// 	TutorialFile string      `json:"tutorial_file"`
// }

type HistoryResult struct {
	Id       int    `json:"id"`
	Date     string `json:"date"`
	Quantity int    `json:"quantity"`
}

type TutorialResult struct {
	IdService   int    `json:"id_service"`
	Name        string `json:"name"`
	Description string `json:"description"`
	FileUrl     string `json:"file_url"`
}
