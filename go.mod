module projek

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/fsnotify/fsnotify v1.4.9
	github.com/jinzhu/gorm v1.9.16
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/nats-io/nats.go v1.10.0
	github.com/spf13/viper v1.7.1
	github.com/valyala/fasttemplate v1.2.1 // indirect
	go.uber.org/zap v1.16.0
)
