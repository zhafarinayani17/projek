package properties

import (
	"fmt"
	"projek/gitlab.com/notula/go-tools/dbms"
)

type ServiceProperties struct {
	Nats            NatsProperties          `json:"nats" mapstructure:"nats"`
	Logging         LoggingRotateProperties `json:"logging" mapstructure:"logging"`
	Database        *dbms.Properties        `json:"database"  mapstructure:"database"`
	Topic           NatsTopicProperties     `json:"topic" mapstructure:"topic"`
	TranscribeLimit int                     `json:"transcribe_limit" mapstructure:"transcribe_limit"`
	TotalSummary    int                     `json:"total_summary" mapstructure:"total_summary"`
	AvatarPath      string                  `json:"avatar_path"  mapstructure:"avatar_path"`
	FilePath        string                  `json:"file_path" mapstructure:"file_path"`
}

type ServiceRotateProperties struct {
	Nats            NatsProperties          `json:"nats" mapstructure:"nats"`
	Logging         LoggingRotateProperties `json:"logging" mapstructure:"logging"`
	Database        *dbms.Properties        `json:"database"  mapstructure:"database"`
	Topic           NatsTopicProperties     `json:"topic" mapstructure:"topic"`
	TranscribeLimit int                     `json:"transcribe_limit" mapstructure:"transcribe_limit"`
	TotalSummary    int                     `json:"total_summary" mapstructure:"total_summary"`
	AvatarPath      string                  `json:"avatar_path"  mapstructure:"avatar_path"`
	FilePath        string                  `json:"file_path" mapstructure:"file_path"`
}

type LoggingRotateProperties struct {
	OutputDir      string `json:"output_dir" mapstructure:"output_dir"`
	ErrorOutputDir string `json:"error_output_dir" mapstructure:"error_output_dir"`
	Name           string `json:"name" mapstructure:"name"`
	Debug          bool   `json:"debug" mapstructure:"debug"`
}

type NatsTopicProperties struct {
	BaseURL            string                       `json:"base_url" mapstructure:"base_url"`
	EnvirontmentStatus string                       `json:"environtment_status" mapstructure:"environtment_status"`
	Channel            map[string]ChannelProperties `json:"channel" `
}

type RouterEndPointProperties struct {
	RestBaseUrl        string                                          `json:"rest_base_url" mapstructure:"rest_base_url"`
	NatsBaseUrl        string                                          `json:"nats_base_url" mapstructure:"nats_base_url"`
	EnvirontmentStatus string                                          `json:"environtment_status" mapstructure:"environtment_status"`
	Channel            map[string]map[string]ChannelEndpointProperties `json:"channel" `
}

func (this *NatsTopicProperties) GetChannel(name string) string {
	return fmt.Sprintf("%s.%s.%s", this.BaseURL, this.EnvirontmentStatus, this.Channel[name].Path)
}

func (this *RouterEndPointProperties) GetChannelRest(name string, types string) string {
	return fmt.Sprintf("%s/%s/%s", this.RestBaseUrl, this.EnvirontmentStatus, this.Channel[types][name].RestPath)
}
func (this *RouterEndPointProperties) GetChannelNats(name string, types string) string {
	return fmt.Sprintf("%s.%s.%s", this.NatsBaseUrl, this.EnvirontmentStatus, this.Channel[types][name].NatsPath)
}

func (this *NatsTopicProperties) GetQueueName(name string) string {
	return this.Channel[name].QueueName
}

func (this *NatsTopicProperties) GetDurableName(name string) string {
	return this.Channel[name].DurableName
}

func (this *NatsTopicProperties) GetQueueSubscribeProperties(name string) (string, string) {
	return this.GetChannel(name), this.GetQueueName(name)
}

type ChannelProperties struct {
	Path        string `json:"path" mapstructure:"path"`
	QueueName   string `json:"queue_name" mapstructure:"queue_name"`
	DurableName string `json:"durable_name" mapstructure:"durable_name"`
}

type ChannelEndpointProperties struct {
	RestPath string `json:"rest_path" mapstructure:"rest_path"`
	NatsPath string `json:"nats_path" mapstructure:"nats_path"`
}

type NatsProperties struct {
	Address   string `json:"address" mapstructure:"address" `
	ClusterId string `json:"cluster_id" mapstructure:"cluster_id"`
	ClientId  string `json:"client_id" mapstructure:"client_id"`
}

type LoggingProperties struct {
	OutputPaths      []string `json:"output_paths" mapstructure:"output_paths"`
	ErrorOutputPaths []string `json:"error_output_paths" mapstructure:"error_output_paths"`
}

type EndPointProperties struct {
	Nats    NatsProperties           `json:"nats" mapstructure:"nats"`
	Logging LoggingProperties        `json:"logging" mapstructure:"logging"`
	Topic   RouterEndPointProperties `json:"topic" mapstructure:"topic"`
	Port    string                   `json:"port" mapstructure:"port"`
	Timeout int                      `json:"timeout" mapstructure:"timeout"`
	CertDir string                   `json:"cert_dir" mapstructure:"cert_dir"`
}
